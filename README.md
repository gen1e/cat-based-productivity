# What is this?

This is an app meant to keep you productive using cats as a reward. You do a task you get x amount of cat pictures to look at. Trying to harness the power of the social media scroll for good. This is mainly a personal side project I'm using to keep my android skills sharp, don't expect much of it. Licensed under GNU GPLv3.
